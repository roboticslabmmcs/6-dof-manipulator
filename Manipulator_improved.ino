#define SERVO_DELAY 1700            //Задержка на выполнение плавного поворота
#define START_DELAY 3000            //Задержка на инициализацию манипулятора
#define CORRECT 55                  //Признак корректности в EEPROM

#define RELAY 8                     //Порт для реле

#define VERSION "MANIPULATOR v1.0"
#define MAX_BT_COMMAND_LENGTH 30         //Максимальный размер команды, принятой из COM-порта

#include <MsTimer2.h>
#include <Servo.h>
#include <EEPROM.h>

const int timerInMillies = 60;         //Период вызова таймера

int control_value = 0;              //Число корректности данных в EEPROM
int byte_position = 0;              //Позиция байта в EEPROM

char command_buf[MAX_BT_COMMAND_LENGTH];           //Буфер для данных из COM-порта
int angle;                                         //Угол для сервомашинки, принятый из COM-порта

const int countSinus = 34;          //Количетво синусов в таблице
const int MainSinusTable[] = {          //Основная таблица синусов
  0, 0, 0, 0, 1, 2,
  5, 8, 12, 17, 24, 31,
  40, 49, 60, 71, 82, 93,
  105, 116, 126, 136, 145, 153,
  160, 166, 171, 174, 177, 179,
  180, 180, 180, 180
};

int Sinus_1[countSinus];         //Масштабированные таблицы синусов для каждой сервы
int Sinus_2[countSinus];
int Sinus_3[countSinus];
int Sinus_4[countSinus];
int Sinus_5[countSinus];
int Sinus_6[countSinus];

int ServoState[2][6];           //Матрица состояний для сервомашинки

int Index_1;              //Индексация таблицы синусов для каждой сервы
bool rotate_dir_1 = 0;    //Флаг направления движения сервы    0: 0->180  ***  1: 180->0

int Index_2;
bool rotate_dir_2 = 0;

int Index_3;
bool rotate_dir_3 = 0;

int Index_4;
bool rotate_dir_4 = 0;

int Index_5;
bool rotate_dir_5 = 0;

int Index_6;
bool rotate_dir_6 = 0;

bool block_1 = false;                //Флаги блокировки движения для серв
bool block_2 = false;
bool block_3 = false;
bool block_4 = false;
bool block_5 = false;
bool block_6 = false;

Servo Servo_1;                               //Объявление серв
Servo Servo_2;
Servo Servo_3;
Servo Servo_4;
Servo Servo_5;
Servo Servo_6;

int Get_Current_Angle(int number_of_servo)      //Функция получения текущего угла поворота сервы, параметр - номер сервы
{
  switch (number_of_servo)
  {
    case 1:
      return ServoState[0][0];
      break;
    case 2:
      return ServoState[0][1];
      break;
    case 3:
      return ServoState[0][2];
      break;
    case 4:
      return ServoState[0][3];
      break;
    case 5:
      return ServoState[0][4];
      break;
    case 6:
      return ServoState[0][5];
      break;
  }
}

void Servo_write(int servo_number, int angle)
{
  switch (servo_number)
  {
    case 1:
      Servo_1.write(angle);
      ServoState[0][0] = angle;
      break;
    case 2:
      Servo_2.write(angle);
      ServoState[0][1] = angle;
      break;
    case 3:
      Servo_3.write(angle);
      ServoState[0][2] = angle;
      break;
    case 4:
      Servo_4.write(angle);
      ServoState[0][3] = angle;
      break;
    case 5:
      Servo_5.write(angle);
      ServoState[0][4] = angle;
      break;
    case 6:
      Servo_6.write(angle);
      ServoState[0][5] = angle;
      break;
  }
  ServoState[0][servo_number - 1] = angle; //Обновление матрицы состояний
}

void Turn_Servos(int angle_1, int angle_2, int angle_3, int angle_4, int angle_5, int angle_6)       //Функция параллельного поворота всех серв на произвольный угол, не поворачивать: -1
{
  if (angle_1 == -1)
    ServoState[1][0] = ServoState[0][0];
  else
    ServoState[1][0] = angle_1;

  if (angle_2 == -1)
    ServoState[1][1] = ServoState[0][1];
  else
    ServoState[1][1] = angle_2;

  if (angle_3 == -1)
    ServoState[1][2] = ServoState[0][2];
  else
    ServoState[1][2] = angle_3;

  if (angle_4 == -1)
    ServoState[1][3] = ServoState[0][3];
  else
    ServoState[1][3] = angle_4;

  if (angle_5 == -1)
    ServoState[1][4] = ServoState[0][4];
  else
    ServoState[1][4] = angle_5;

  if (angle_6 == -1)
    ServoState[1][5] = ServoState[0][5];
  else
    ServoState[1][5] = angle_6;

  Turn();
}

void Turn_Servo(int number_of_servo, int angle)      //Функция поворота одной сервы на угол, параметры: номер сервы, угол
{
  switch (number_of_servo)
  {
    case 1:
      Turn_Servos(angle, -1, -1, -1, -1, -1);
      break;
    case 2:
      Turn_Servos(-1, angle, -1, -1, -1, -1);
      break;
    case 3:
      Turn_Servos(-1, -1, angle, -1, -1, -1);
      break;
    case 4:
      Turn_Servos(-1, -1, -1, angle, -1, -1);
      break;
    case 5:
      Turn_Servos(-1, -1, -1, -1, angle, -1);
      break;
    case 6:
      Turn_Servos(-1, -1, -1, -1, -1, angle);
      break;
  }
}

void move()                        //Функция, периодично вызываемая таймером. Выполянет плавное движение
{
  ///// **** 1 **** /////
  if (block_1 == false)                       //Проверка блокировки сервы
  {
    if (rotate_dir_1 == 0)                    //Проверка направления движения
    {
      //Serial1.println(Sinus_1[Index_1]);
      Servo_1.write(Sinus_1[Index_1]);        //Поворот на табличный угол
      Index_1++;
      if (Index_1 == countSinus - 1)          //Проверка конца таблицы углов
      {
        MsTimer2::stop();
        return;
      }
    }
    else
    {
      //Serial1.println(Sinus_1[Index_1]);
      Servo_1.write(Sinus_1[Index_1]);
      Index_1--;
      if (Index_1 == -1)
      {
        MsTimer2::stop();
        return;
      }
    }
  }

  ///// **** 2 **** /////
  if (block_2 == false)
  {
    if (rotate_dir_2 == 0)
    {
      //Serial1.println(Sinus_2[Index_2]);
      Servo_2.write(Sinus_2[Index_2]);
      Index_2++;
      if (Index_2 == countSinus - 1)
      {
        MsTimer2::stop();
        return;
      }
    }
    else
    {
      //Serial1.println(Sinus_2[Index_2]);
      Servo_2.write(Sinus_2[Index_2]);
      Index_2--;
      if (Index_2 == -1)
      {
        MsTimer2::stop();
        return;
      }
    }
  }

  ///// **** 3 **** /////
  if (block_3 == false)
  {
    if (rotate_dir_3 == 0)
    {
      //Serial1.println(Sinus_3[Index_3]);
      Servo_3.write(Sinus_3[Index_3]);
      Index_3++;
      if (Index_3 == countSinus - 1)
      {
        MsTimer2::stop();
        return;
      }
    }
    else
    {
      //Serial1.println(Sinus_3[Index_3]);
      Servo_3.write(Sinus_3[Index_3]);
      Index_3--;
      if (Index_3 == -1)
      {
        MsTimer2::stop();
        return;
      }
    }
  }

  ///// **** 4 **** /////
  if (block_4 == false)
  {
    if (rotate_dir_4 == 0)
    {
      //Serial1.println(Sinus_4[Index_4]);
      Servo_4.write(Sinus_4[Index_4]);
      Index_4++;
      if (Index_4 == countSinus - 1)
      {
        MsTimer2::stop();
        return;
      }
    }
    else
    {
      //Serial1.println(Sinus_4[Index_4]);
      Servo_4.write(Sinus_4[Index_4]);
      Index_4--;
      if (Index_4 == -1)
      {
        MsTimer2::stop();
        return;
      }
    }
  }

  ///// **** 5 **** /////
  if (block_5 == false)
  {
    if (rotate_dir_5 == 0)
    {
      //Serial1.println(Sinus_5[Index_5]);
      Servo_5.write(Sinus_5[Index_5]);
      Index_5++;
      if (Index_5 == countSinus - 1)
      {
        MsTimer2::stop();
        return;
      }
    }
    else
    {
      //Serial1.println(Sinus_5[Index_5]);
      Servo_5.write(Sinus_5[Index_5]);
      Index_5--;
      if (Index_5 == -1)
      {
        MsTimer2::stop();
        return;
      }
    }
  }

  ///// **** 6 **** /////
  if (block_6 == false)
  {
    if (rotate_dir_6 == 0)
    {
      //Serial1.println("Forward");
      //Serial1.println(Sinus_6[Index_6]);
      Servo_6.write(Sinus_6[Index_6]);
      Index_6++;
      if (Index_6 == countSinus - 1)
      {
        MsTimer2::stop();
        return;
      }
    }
    else
    {
      //Serial1.println("Backward");
      //Serial1.println(Sinus_6[Index_6]);
      Servo_6.write(Sinus_6[Index_6]);
      Index_6--;
      if (Index_6 == -1)
      {
        MsTimer2::stop();
        return;
      }
    }
  }
}

void Turn()                       //Функция, проводящая вычисление таблиц углов для всех серв и вызывающая таймер
{
  //////// **** 1 **** ////////
  if (ServoState[0][0] < ServoState[1][0])   //Вычисление направления движения
  {
    Index_1 = 0;
    rotate_dir_1 = 0;
  }
  else
  {
    Index_1 = 33;
    rotate_dir_1 = 1;
  }

  //////// **** 2 **** ////////
  if (ServoState[0][1] < ServoState[1][1])
  {
    Index_2 = 0;
    rotate_dir_2 = 0;
  }
  else
  {
    Index_2 = 33;
    rotate_dir_2 = 1;
  }

  //////// **** 3 **** ////////
  if (ServoState[0][2] < ServoState[1][2])
  {
    Index_3 = 0;
    rotate_dir_3 = 0;
  }
  else
  {
    Index_3 = 33;
    rotate_dir_3 = 1;
  }

  //////// **** 4 **** ////////
  if (ServoState[0][3] < ServoState[1][3])
  {
    Index_4 = 0;
    rotate_dir_4 = 0;
  }
  else
  {
    Index_4 = 33;
    rotate_dir_4 = 1;
  }

  //////// **** 5 **** ////////
  if (ServoState[0][4] < ServoState[1][4])
  {
    Index_5 = 0;
    rotate_dir_5 = 0;
  }
  else
  {
    Index_5 = 33;
    rotate_dir_5 = 1;
  }

  //////// **** 6 **** ////////
  if (ServoState[0][5] < ServoState[1][5])
  {
    Index_6 = 0;
    rotate_dir_6 = 0;
  }
  else
  {
    Index_6 = 33;
    rotate_dir_6 = 1;
  }

  //////// **** 1 **** ////////
  if (ServoState[0][0] != ServoState[1][0])           //Проверка равенства конечного и начального углов
  {
    block_1 = false;
    int delta_1 = abs(ServoState[1][0] - ServoState[0][0]);      //Поворотный угол
    float factor_1 = (float)180 / (float)delta_1;                                //Отношение углов
    for (int i = 0; i < countSinus; i++)                                //Актуальная таблица углов (маштабированная таблица синусов)
      Sinus_1[i] = min(ServoState[0][0], ServoState[1][0]) + (MainSinusTable[i] / factor_1);
  }
  else
    block_1 = true;

  //////// **** 2 **** ////////
  if (ServoState[0][1] != ServoState[1][1])
  {
    block_2 = false;
    int delta_2 = abs(ServoState[1][1] - ServoState[0][1]);
    float factor_2 = (float)180 / (float)delta_2;
    for (int i = 0; i < countSinus; i++)
      Sinus_2[i] = min(ServoState[0][1], ServoState[1][1]) + (MainSinusTable[i] / factor_2);
  }
  else
    block_2 = true;

  //////// **** 3 **** ////////
  if (ServoState[0][2] != ServoState[1][2])
  {
    block_3 = false;
    int delta_3 = abs(ServoState[1][2] - ServoState[0][2]);
    float factor_3 = (float)180 / (float)delta_3;
    for (int i = 0; i < countSinus; i++)
      Sinus_3[i] = min(ServoState[0][2], ServoState[1][2]) + (MainSinusTable[i] / factor_3);
  }
  else
    block_3 = true;

  //////// **** 4 **** ////////
  if (ServoState[0][3] != ServoState[1][3])
  {
    block_4 = false;
    int delta_4 = abs(ServoState[1][3] - ServoState[0][3]);
    float factor_4 = (float)180 / (float)delta_4;
    for (int i = 0; i < countSinus; i++)
      Sinus_4[i] = min(ServoState[0][3], ServoState[1][3]) + (MainSinusTable[i] / factor_4);
  }
  else
    block_4 = true;

  //////// **** 5 **** ////////
  if (ServoState[0][4] != ServoState[1][4])
  {
    block_5 = false;
    int delta_5 = abs(ServoState[1][4] - ServoState[0][4]);
    float factor_5 = (float)180 / (float)delta_5;
    for (int i = 0; i < countSinus; i++)
      Sinus_5[i] = min(ServoState[0][4], ServoState[1][4]) + (MainSinusTable[i] / factor_5);
  }
  else
    block_5 = true;


  //////// **** 6 **** ////////
  if (ServoState[0][5] != ServoState[1][5])
  {
    block_6 = false;
    int delta_6 = abs(ServoState[1][5] - ServoState[0][5]);
    float factor_6 = (float)180 / (float)delta_6;
    for (int i = 0; i < countSinus; i++)
      Sinus_6[i] = min(ServoState[0][5], ServoState[1][5]) + (MainSinusTable[i] / factor_6);
  }
  else
    block_6 = true;

  MsTimer2::start();         //Старт таймера

  ServoState[0][0] = ServoState[1][0];           //Обновление текущих углов в матрице состояний
  ServoState[0][1] = ServoState[1][1];
  ServoState[0][2] = ServoState[1][2];
  ServoState[0][3] = ServoState[1][3];
  ServoState[0][4] = ServoState[1][4];
  ServoState[0][5] = ServoState[1][5];

  delay(SERVO_DELAY);                   //Задержка на выполнение
}

void Center()           //Центровка всех серв
{
  Turn_Servos(90, 90, 90, 90, 90, 110);
}

void CheckCommand()           //Процедура проверки COM-порта и парсинга команд
{
  String command_name = GetCommand();
  if (command_name.length() > 0)
  {
    Serial1.flush();
    if (command_name == "CENTER")
    {
      Center();
      Serial1.println("Center confirmed");
    }
    else if (command_name == "PARK")
    {
      Serial1.println("Park confirmed");

      digitalWrite(RELAY, LOW);
      Serial1.println("Relay disengaged");

      EEPROM.write(0, CORRECT);           //Запись первого байта корректности
      for (byte_position = 1; byte_position < 7; byte_position++)        //Проход по байтам для серв
        EEPROM.update(byte_position, Get_Current_Angle(byte_position));  //Обновление текущих углов в EEPROM
    }
    else if (command_name == "STATUS")
    {
      Serial1.println("Current manipulator status: ");
      Serial1.println(VERSION);
      Serial1.println("Servo 1 angle: " + String(Get_Current_Angle(1)));
      Serial1.println("Servo 2 angle: " + String(Get_Current_Angle(2)));
      Serial1.println("Servo 3 angle: " + String(Get_Current_Angle(3)));
      Serial1.println("Servo 4 angle: " + String(Get_Current_Angle(4)));
      Serial1.println("Servo 5 angle: " + String(Get_Current_Angle(5)));
      Serial1.println("Servo 6 angle: " + String(Get_Current_Angle(6)));
    }
    else
    {
      String servo_select = command_name.substring(0, 1);                      //Номер сервы в string
      String servo_angle = command_name.substring(1, command_name.length());   //Угол для сервы в string

      if (servo_select == "A")
      {
        angle = servo_angle.toInt();
        Serial1.println("Servo 1 angle " + String(angle) + " confirmed");
        Servo_1.write(angle);
        ServoState[0][0] = angle;
      }
      else if (servo_select == "B")
      {
        angle = servo_angle.toInt();
        Serial1.println("Servo 2 angle " + String(angle) + " confirmed");
        Servo_2.write(angle);
        ServoState[0][1] = angle;
      }
      else if (servo_select == "C")
      {
        angle = servo_angle.toInt();
        Serial1.println("Servo 3 angle " + String(angle) + " confirmed");
        Servo_3.write(angle);
        ServoState[0][2] = angle;
      }
      else if (servo_select == "D")
      {
        angle = servo_angle.toInt();
        Serial1.println("Servo 4 angle " + String(angle) + " confirmed");
        Servo_4.write(angle);
        ServoState[0][3] = angle;
      }
      else if (servo_select == "E")
      {
        angle = servo_angle.toInt();
        Serial1.println("Servo 5 angle " + String(angle) + " confirmed");
        Servo_5.write(angle);
        ServoState[0][4] = angle;
      }
      else if (servo_select == "F")
      {
        angle = servo_angle.toInt();
        Serial1.println("Servo 6 angle " + String(angle) + " confirmed");
        Servo_6.write(angle);
        ServoState[0][5] = angle;
      }
      else if (servo_select == "S")
      {
        Serial1.println("Smooth movement confirmed");

        int count = 0;         //Число цифр - длина угла
        int i = 1;
        int angles[6];
        int array_pos = 0;

        while (servo_angle[i] != '\0')
        {
          if (i == 1 && servo_angle[i] == ' ' && servo_angle[i - 1] != ' ')
            count++;

          if (servo_angle[i] == ' ' && servo_angle[i - 1] != ' ')  //Условие окончания слова
          {
            String temp = servo_angle.substring(i - count, i);   //Получение подстроки
            angles[array_pos] = temp.toInt();
            array_pos++;

            count = 0;
          }

          if (i == 1 && servo_angle[i] != ' ' && servo_angle[i - 1] != ' ')  //Дополнительное увеличение счетчика, если перед первым словом нет пробелов
            count++;

          if (servo_angle[i] != ' ')   //Увеличение счетчика букв
            count++;

          i++;
        }

        if (i == servo_angle.length() && servo_angle[i] != ' ')    //Если слово последнее и после него нет пробелов
        {
          String temp = servo_angle.substring(i - count, i);   //Получение подстроки
          angles[array_pos] = temp.toInt();
        }
        Serial1.println("Angle 1: " + String(angles[0]));
        Serial1.println("Angle 2: " + String(angles[1]));
        Serial1.println("Angle 3: " + String(angles[2]));
        Serial1.println("Angle 4: " + String(angles[3]));
        Serial1.println("Angle 5: " + String(angles[4]));
        Serial1.println("Angle 6: " + String(angles[5]));
        
        Turn_Servos(angles[0], angles[1], angles[2], angles[3], angles[4], angles[5]);
      }
    }
  }
  Serial1.flush();
}


String GetCommand()          //Получение команды из COM-порта (bluetooth), если команды нет, то возвращается пустая строка
{
  int inputCount = Serial1.available();
  if (inputCount > 0)
  {
    int totalByte = Serial1.readBytesUntil('\n', command_buf, MAX_BT_COMMAND_LENGTH);
    String command = command_buf;
    command = command.substring(0, totalByte);
    command.replace("\r", "");
    command.replace("\n", "");
    return command;
  }
  else
    return "";
}

void setup()
{
  Serial1.begin(115200);
  Serial1.println(VERSION);
  Serial1.println("CONNECTED");

  Servo_1.attach(2);
  Servo_2.attach(3);
  Servo_3.attach(4);
  Servo_4.attach(5);
  Servo_5.attach(6);
  Servo_6.attach(7);

  pinMode(0, INPUT);
  pinMode(1, OUTPUT);

  pinMode(RELAY, OUTPUT);       //Инициализация реле

  ServoState[0][0] = 90;      //Текущий угол
  ServoState[1][0] = -1;     //Конечный угол - null
  ServoState[0][1] = 90;
  ServoState[1][1] = -1;
  ServoState[0][2] = 90;
  ServoState[1][2] = -1;
  ServoState[0][3] = 90;
  ServoState[1][3] = -1;
  ServoState[0][4] = 90;
  ServoState[1][4] = -1;
  ServoState[0][5] = 180;
  ServoState[1][5] = -1;
  /*
  control_value = EEPROM.read(byte_position);    //Получение контрольного байта
  EEPROM.write(0, 0);

  if (control_value != CORRECT)                  //Контрольный байт неверен, вывод серв в стандартное положение
  {
    Servo_1.write(90);
    Servo_2.write(90);
    Servo_3.write(90);
    Servo_4.write(90);
    Servo_5.write(90);
    Servo_6.write(170);
  }
  else
  {
    for (byte_position = 1; byte_position < 7; byte_position++)     //Байт верен, чтение положения серв из EEPROM
    {
      angle = EEPROM.read(byte_position);       //Текущий угол из EEPROM
      Servo_write(byte_position, angle);        //Разворот сервы на угол
    }
  }
  */
  //Отправка текущих положений серв
  Serial1.println(String(Get_Current_Angle(1)) + " " + String(Get_Current_Angle(2)) + " " + String(Get_Current_Angle(3)) + " " + String(Get_Current_Angle(4)) + " " + String(Get_Current_Angle(5)) + " " + String(Get_Current_Angle(6)));

  delay(START_DELAY);
  digitalWrite(RELAY, HIGH);             //Активация реле => сервомашинок
  Serial1.println("Relay engaged");

  MsTimer2::set(timerInMillies, move);   //move вызывается каждый промежуток времени
  delay(START_DELAY);
}

void loop()
{
  CheckCommand();
}

